import React, { useState } from 'react';
import './SignIn.css'; 
import { Link, Navigate, useNavigate } from 'react-router-dom';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';


const SignIn = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const navigate = useNavigate();

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      if(checklogin(email, password)){
        const hashedPassword= hashPassword(password);
        await login([email, hashedPassword]);
      };

    } catch (error) {
        console.error('Erreur de connexion:', error.message);
    }
    navigate('/');
  };
  const checklogin = async (email, password) => {

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
        throw new Error('Veuillez saisir une adresse e-mail valide.');
    }

    return true; 
  };

  const login = async (postData) => {
    try {
      const response = await fetch('https://71e0-134-214-214-154.ngrok-free.app/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(postData),
      });

      if (!response.ok) {
        throw new Error('Erreur lors de la requête POST');
      }

      const responseData = await response.json();
      console.log('Réponse de l\'API:', responseData);
    } catch (error) {
      console.error('Erreur lors de la requête POST:', error.message);
    }
  };
  const hashPassword = async (password) => {
    const data = `${password}`;
  
    const encoder = new TextEncoder();
    const messageBuffer = encoder.encode(data);
    const hashBuffer = await crypto.subtle.digest('SHA-256', messageBuffer);
  
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashedPassword = hashArray.map(byte => byte.toString(16).padStart(2, '0')).join('');
  
    return hashedPassword;
  };

  return (
    <div className='wrapper'>
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <div className='formTextField'>
          <TextField className='TextField' id="outlined-basic" label="Email *" variant="outlined" type="email" value={email} onChange={handleEmailChange} />
          <TextField className='TextField' id="outlined-basic" label="Password *" variant="outlined" type="password" value={password} onChange={handlePasswordChange} />
        </div>

        <Button type="submit" variant="contained">Sign In</Button>
        <Link className='register' to='/register'>Don't have an account ? Register now</Link>
      </form>
    </div>
  );
};

export default SignIn;
