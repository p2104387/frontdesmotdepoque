import React, { Children } from "react";
import "./BotResponse.css";

const BotResponse = ({ children }) => {
  return <div className="wrapperBotResponse">{children}</div>;
};

export default BotResponse;
