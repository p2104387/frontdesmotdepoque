import React from "react";
import "./Character.css";
import ActionCharacter from "../ActionCharacter/ActionCharacter";

const Character = (props) => {
  return (
    <div className="wrapperCharacter">
      <div className="pictureContainer">
        {props.image && (
          <img className="picture" src={props.image} alt={props.name} />
        )}
      </div>
      <span className="name">{props.name}</span>
      <ActionCharacter onDelete={props.onDelete} onDuplicate={props.onDuplicate} onEdit={props.onEdit} />
    </div>
  );
};

export default Character;
