import React, { useState } from 'react';
import './Register.css';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';


import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

const Register = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const navigate = useNavigate();

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      // Supposons que vous avez une fonction `register` pour l'inscription
      // (vous devez implémenter votre propre logique ici)
      if(checkregister(email, password)){
        const hashedPassword= hashPassword(password);
        await SignUp([email, hashedPassword]);
      };

      // Redirigez l'utilisateur vers la page de connexion après l'inscription
    } catch (error) {
        console.error('Erreur de création de compte:', error.message);
    }
    navigate('/login');
  };
  const checkregister = async (email, password) => {

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
        throw new Error('Veuillez saisir une adresse e-mail valide.');
    }
  
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&-])[A-Za-z\d@$!%*?&-]{8,}$/;
    if (!passwordRegex.test(password)) {
      throw new Error('Le mot de passe doit contenir au moins une majuscule, une minuscule, un chiffre et un caractère spécial, et au moins 8 caractères.');
    }
    return true;
    
  };
  const SignUp = async (postData) => {
    try {
      const response = await fetch('https://71e0-134-214-214-154.ngrok-free.app/user/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(postData),
      });

      if (!response.ok) {
        throw new Error('Erreur lors de la requête POST');
      }

      const responseData = await response.json();
      console.log('Réponse de l\'API:', responseData);
    } catch (error) {
      console.error('Erreur lors de la requête POST:', error.message);
    }
  };
  const hashPassword = async (password) => {
    // Concaténez le mot de passe et le sel
    const data = `${password}`;
  
    // Utilisez la fonction de hachage SHA-256
    const encoder = new TextEncoder();
    const messageBuffer = encoder.encode(data);
    const hashBuffer = await crypto.subtle.digest('SHA-256', messageBuffer);
  
    // Convertissez le haché en une chaîne hexadécimale
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashedPassword = hashArray.map(byte => byte.toString(16).padStart(2, '0')).join('');
  
    return hashedPassword;
  };

  return (
    <div className='wrapperRegister'>
      <h1>Sign Up</h1>
      <form onSubmit={handleSubmit}>
        <div className='formTextField'>
          <TextField className='TextField' id="outlined-basic" label="Email *" variant="outlined" type="email" value={email} onChange={handleEmailChange} />
          <TextField className='TextField' id="outlined-basic" label="Password *" variant="outlined" type="password" value={password} onChange={handlePasswordChange}  />
        </div>
        <Button type="submit" variant="contained">Sign Up</Button>
        <Link className='register' to='/login'>Already have an account ? Login here</Link>
      </form>
    </div>
  );
};

export default Register;

