import React from "react";
import "./AddCharacter.css";

function validateString(inputString) {

  if (inputString.length > 500) {
    console.error("La chaîne est trop longue (maximum 500 caractères).");
    return false;
  }

  //les caractères autorisés a à z,A à Z,0 à 9 et \s.,!?'"()/-
  const allowedCharacters = /^[a-zA-Z0-9\s.,!?'"()/-]+$/;
  if (!allowedCharacters.test(inputString)) {
    console.error("La chaîne contient des caractères non autorisés.");
    return false;
  }
  return true;
}

const AddCharacter = (props) => {
  return (
    <div>
        <form onSubmit={props.handleSubmit}>
            <label htmlFor="name">Name:</label>
            <input
            type="text"
            name="name"
            id="name"
            value={props.name}
            onChange={props.handleChange}
            />
            <label htmlFor="image">Image:</label>
            <input
            type="text"
            name="image"
            id="image"
            value={props.image}
            onChange={props.handleChange}
            />
            <input type="submit" value="Add Character" />
        </form>
    </div>
  );
};

export default AddCharacter;
