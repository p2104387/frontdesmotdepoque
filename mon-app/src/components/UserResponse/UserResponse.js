import React, { Children } from "react";
import "./UserResponse.css";

const UserResponse = ({ children }) => {
  return <div className="wrapperUserResponse">{children}</div>;
};

export default UserResponse;
