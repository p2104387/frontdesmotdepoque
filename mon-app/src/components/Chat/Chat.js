import React, { useState } from "react";
import "./Chat.css";
import Character from "../Character/Character";
import charactersData from "../../test/character.json";
import discussionsData from "../../test/discussion.json";
import TextField from "@mui/material/TextField";
import BotResponse from "../BotResponse/BotResponse";
import UserResponse from "../UserResponse/UserResponse";
import { MdOutlineAddToPhotos } from "react-icons/md";
import { IoSend } from "react-icons/io5";
import { Md } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import Profil from "../Profil/Profil";
import conf from "../../Config.json";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";

function Chat() {
  const [characters, setCharacters] = useState(charactersData.characters);
  const navigate = useNavigate();
  const [selectedCharacter, setSelectedCharacter] = useState(null);
  const [editingCharacter, setEditingCharacter] = useState(null);
  const [isEditModalOpen, setEditModalOpen] = useState(false);
  const [newCharacterName, setNewCharacterName] = useState("");
  const [currentConversation, setCurrentConversation] = useState([]);

  const [message, setMessage] = useState("");

  const logo = conf.img.logo;

  function handleCharacterClick(characterName) {
    setSelectedCharacter(characterName);

    // Chargez la conversation actuelle à partir du fichier JSON ou de votre source de données
    const currentCharacter = discussionsData.characters.find(
      (c) => c.name === characterName
    );
    setCurrentConversation(currentCharacter ? currentCharacter.messages : []);
  }

  function deleteCharacter(characterName) {
    const updatedCharacters = characters.filter(
      (char) => char !== characterName
    );
    setCharacters(updatedCharacters);
    setSelectedCharacter(null);
  }

  function duplicateCharacter(characterName) {
    const duplicatedCharacter = characterName + "_cp";
    setCharacters((prevCharacters) => [...prevCharacters, duplicatedCharacter]);
    setSelectedCharacter(null);
  }

  function editCharacter(characterName) {
    setEditingCharacter(characterName);
    setNewCharacterName(characterName);
    setEditModalOpen(true);
  }

  const handleEditModalClose = () => {
    setNewCharacterName("");
    setEditModalOpen(false);
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      sendMessage();
    }
  };

  const sendMessage = () => {
    if (message.trim() !== "") {
      const newMessage = { sender: "Player", content: message };

      // Mettez à jour la conversation actuelle
      setCurrentConversation((prevMessages) => [...prevMessages, newMessage]);

      // (Optionnel) Mettez à jour le fichier JSON ou votre source de données
      // (Cela dépend de la manière dont vous chargez initialement vos données)
      // Pour cet exemple, vous pouvez le faire côté client uniquement et cela ne sera pas persistant
      // Pour une persistance réelle, vous devriez utiliser une API ou un backend
      console.log(
        "Mises à jour sauvegardées dans le fichier JSON:",
        currentConversation
      );

      // Réinitialisez l'input après l'envoi
      setMessage("");
    }
  };

  const handleInputChange = (e) => {
    setMessage(e.target.value);
  };

  const handleEditModalSave = () => {
    // Vérification du nouveau nom
    const isValidNewName = /^[a-zA-Z0-9]{1,15}$/.test(newCharacterName);

    if (isValidNewName) {
      const updatedCharacters = characters.map((char) =>
        char === editingCharacter ? newCharacterName : char
      );
      setCharacters(updatedCharacters);
      setEditingCharacter(null);
      setEditModalOpen(false);
    } else {
      throw new Error(
        "Le nouveau nom doit contenir uniquement des lettres ou des chiffres et avoir une longueur maximale de 15 caractères."
      );
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img className="logo" src={logo} alt="Logo"></img>
        <p>Des Mots D'Époque</p>
        <Profil className="Profil" />
      </header>
      <div className="page">
        <div className="RigthMenu">
          <div className="MenuTop">
            <div
              className="addCharacter"
              onClick={() => navigate("/addcharacter")}
            >
              Ajouter un personnage
              <MdOutlineAddToPhotos />
            </div>
            <div className="bar"></div>
          </div>
          <div className="list-character">
            {characters.map((character, index) => (
              <div
                className="divcharacter"
                key={index}
                onClick={() => handleCharacterClick(character)}
              >
                <Character
                  name={character}
                  onDelete={() => deleteCharacter(character)}
                  onDuplicate={() => duplicateCharacter(character)}
                  onEdit={() => editCharacter(character)}
                />
              </div>
            ))}
          </div>
        </div>
        <div className="chat-container">
          <div className="chat-container-feed">
            <div className="chat-container-feed-content">
              <div className="chat-container-feed-content-reponse">
                {currentConversation.map((message, index) =>
                  message.sender === "Player" ? (
                    <UserResponse key={index}>{message.content}</UserResponse>
                  ) : (
                    <BotResponse key={index}>{message.content}</BotResponse>
                  )
                )}
              </div>
              <div className="chat-container-feed-content-input">
                <input
                  type="text"
                  value={message}
                  onChange={handleInputChange}
                  onKeyPress={handleKeyPress}
                />
                <IoSend className="send" onClick={sendMessage} />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Dialog open={isEditModalOpen} onClose={handleEditModalClose}>
        <DialogTitle>Modifier le nom du personnage</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="newCharacterName"
            label="Nouveau nom"
            fullWidth
            value={newCharacterName}
            onChange={(e) => setNewCharacterName(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleEditModalClose}>Annuler</Button>
          <Button onClick={handleEditModalSave}>Enregistrer</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default Chat;
