import React from 'react';

const CustomButton = (props) => {
    return (
        <button>
            {props.children}
        </button>
    );
};

export default CustomButton;
