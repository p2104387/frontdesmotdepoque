import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import SignIn from './components/SignIn/SignIn';
import Register from './components/Register/Register';
import Chat from './components/Chat/Chat';
import AddCharacter from './components/AddCharacter/AddCharacter';

const App = () => {
  const isAuthenticated = true;

  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={isAuthenticated ? <Chat /> : <Navigate to="/login" />}
        />
        <Route path="/login" element={<SignIn />} />
        <Route path="/register" element={<Register />} />
        <Route path="/addcharacter" element={<AddCharacter />} />
      </Routes>
    </Router>
  );
};

export default App;