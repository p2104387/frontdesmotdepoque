import { checklogin } from '../login';

describe('checklogin Function', () => {
  it('should return true for a valid email', () => {
    const validEmail = 'test@example.com';
    const result = checklogin(validEmail, 'password123');
    expect(result).toBe(true);
  });

  it('should throw an error for an invalid email', () => {
    const invalidEmail = 'invalid-email';
    expect(() => checklogin(invalidEmail, 'password123')).toThrowError('Veuillez saisir une adresse e-mail valide.');
  });

  // Ajoutez d'autres tests en fonction de vos besoins spécifiques
});
