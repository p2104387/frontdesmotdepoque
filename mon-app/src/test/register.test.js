import { register } from '../register';

const register = require('./register');

describe('register function', () => {
  test('should throw an error if the email is invalid', async () => {
    await expect(register('invalid-email', 'ValidPassword1@')).rejects.toThrow();
  });

  test('should throw an error if the password is invalid', async () => {
    await expect(register('valid@example.com', 'weak')).rejects.toThrow();
  });

  test('should not throw an error if both email and password are valid', async () => {
    await expect(register('valid@example.com', 'StrongPassword1@')).resolves.not.toThrow();
  });

  // Ajoutez d'autres tests en fonction de vos besoins spécifiques
});