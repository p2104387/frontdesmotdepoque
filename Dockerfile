FROM node:latest AS build

WORKDIR /app

COPY mon-app/package*.json ./

RUN npm install

COPY mon-app/ .

RUN npm run build

FROM nginx:alpine

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80